const gulp = require('gulp');
const scss = require('gulp-sass');
const plumber = require('gulp-plumber');
const cssnano = require('gulp-cssnano');

//Source Path
const scssSource = 'scss/main.scss';

//Dist Path
const scssDest = 'dist/css/';

//SCSS Task
gulp.task('scss', ()=> {
  console.log('starting scss task');
  return gulp.src(scssSource)
    .pipe(plumber((err) => {
      console.log('scss task error');
      console.log(err);
    }))
    .pipe(scss({
      outputstyle: 'compressed'
    }))
    // .pipe(cssnano())
    .pipe(gulp.dest(scssDest));
});

gulp.task('watch', () => {
  console.log('starting watch task');
  gulp.watch('scss/*/**', ['scss']);
});
